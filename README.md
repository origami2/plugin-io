# Origami 2 plugin socket.io helper

## constructor

Params:

- `api` **required**: Plugin constructor function.
- `privateKey` **required**: Plugin private key.
- `dependenciesMap` **optional**: An object containing string keys and string values. For example:

```
{
  "constructorParameterName": "OtherPluginName"
}
```

  A client object will be created, that client will use the same context as the plugin invoked method.
  Each key like `constructorParameterName` will be mapped to an API proxy.

- `locals` **optional**: An object containing string keys and string values. For example:

```
{
  "param1": 1,
  "param2": "another",
  "db": myDb
}
```
- `events` **optional**:

  Each key like `param1` will be set as the value on the constructor of the same name.
  
> An additional constructor will be passed: `emit` which receives one parameter and broadcast the event to the stack.

## .connect

Returns: a Promise to be resolved once connected
Params:
- `url` **required**: the URL to connect using a [socket.io client](http://socket.io).

> It is expected the other end to be using the [stack socket.io helper](https://gitlab.com/origami2/stack-io)

## .listenSocket

Returns: a Promise to be resolved once connected
Params:
- `socket` **required**: a socket to connect to. Might be an event emitter or a [socket.io socket object](http://socket.io).

> It is expected the other end to be using the [stack socket.io helper](https://gitlab.com/origami2/stack-io)
